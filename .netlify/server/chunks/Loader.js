import { c as create_ssr_component } from "./index2.js";
const Loader_svelte_svelte_type_style_lang = "";
const css = {
  code: '.loading-animation.svelte-28fhdn{width:1.7em;height:1.7em;background-color:rgb(var(--bg-reverse));border-radius:50%;position:relative;animation:svelte-28fhdn-rotate 1000ms linear forwards infinite}.loading-animation.svelte-28fhdn::after{content:"";width:0.7em;height:0.7em;background-color:rgb(var(--bg-color));position:absolute;top:0.3em;left:0.3em;border-radius:50%}@keyframes svelte-28fhdn-rotate{from{transform:rotate(0deg)}to{transform:rotate(360deg)}}',
  map: null
};
const Loader = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  let { isLoading = false } = $$props;
  if ($$props.isLoading === void 0 && $$bindings.isLoading && isLoading !== void 0)
    $$bindings.isLoading(isLoading);
  $$result.css.add(css);
  return `${isLoading ? `<div class="loading-animation svelte-28fhdn"></div>` : ``}`;
});
export {
  Loader as L
};
