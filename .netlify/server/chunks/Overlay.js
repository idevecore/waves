import { c as create_ssr_component, e as escape, v as validate_component, d as add_attribute } from "./index2.js";
import { a as open_mini_audio_control, c as current_radio, d as audio, o as open_audio_control } from "./stores.js";
import { L as Loader } from "./Loader.js";
import "firebase/auth";
import { RadioBrowserApi } from "radio-browser-api";
const Mini_audio_control_svelte_svelte_type_style_lang = "";
const css$2 = {
  code: "main.svelte-1w39j1r.svelte-1w39j1r{width:100%;height:3.5em;background-color:rgb(var(--bg-color-secondary));position:fixed;bottom:0;left:0;display:none;align-items:center;justify-content:center;z-index:2;cursor:pointer;border-top-left-radius:var(--brdr);border-top-right-radius:var(--brdr);box-shadow:rgba(0, 0, 0, 0.02) 0px 1px 3px 0px, rgba(27, 31, 35, 0.15) 0px 0px 0px 1px}main.svelte-1w39j1r .c-image.svelte-1w39j1r{width:3em;height:3em;display:flex;align-items:center;justify-content:center;overflow:hidden}main.svelte-1w39j1r .c-content.svelte-1w39j1r{width:calc(100% - 2em);height:100%;display:flex;align-items:center;justify-content:space-between;padding:var(--pd)}main.svelte-1w39j1r .c-content button.svelte-1w39j1r{width:3.5em;height:3em;border-radius:var(--brdr);border:none;display:flex;align-items:center;justify-content:center;background-color:transparent;cursor:pointer}@media(max-width: 600px){main.svelte-1w39j1r.svelte-1w39j1r{display:flex}}",
  map: null
};
const Mini_audio_control = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  let Open_mini_audio_control = false;
  let Current_radio = {
    name: "",
    favicon: "",
    paused: true,
    loading: false
  };
  const logoRadio = {
    img: ``,
    failed: false,
    loading: false,
    loaded: false
  };
  $$result.css.add(css$2);
  {
    open_mini_audio_control.subscribe((value) => {
      if (Open_mini_audio_control !== value) {
        Open_mini_audio_control = value;
      }
    });
  }
  {
    current_radio.subscribe((value) => {
      Current_radio = value;
      const img = new Image();
      img.src = Current_radio.favicon;
      logoRadio.loading = true;
      img.onload = () => {
        logoRadio.loading = false;
        logoRadio.loaded = true;
        logoRadio.img = `<img width="40" height="40" src="${Current_radio.favicon}"/>`;
      };
      img.onerror = () => {
        logoRadio.loading = false;
        logoRadio.failed = true;
        logoRadio.img = `<i class="bi bi-music-note"></i>`;
      };
    });
  }
  return `${Open_mini_audio_control ? `<main class="svelte-1w39j1r"><div class="c-image svelte-1w39j1r"><!-- HTML_TAG_START -->${logoRadio.img}<!-- HTML_TAG_END --></div>
		<div class="c-content svelte-1w39j1r"><p>${escape(Current_radio.name)}</p>
			<button class="wk-rp svelte-1w39j1r">${Current_radio.loading ? `${validate_component(Loader, "Loader").$$render($$result, { isLoading: true }, {}, {})}` : `${Current_radio.paused ? `<i class="bi bi-play-fill"></i>` : `<i class="bi bi-stop-fill"></i>`}`}</button></div></main>` : ``}`;
});
const Audio_control_svelte_svelte_type_style_lang = "";
const css$1 = {
  code: "main.svelte-ycieuk.svelte-ycieuk.svelte-ycieuk{gap:var(--pd);width:300px;height:100%;z-index:2;padding:var(--pd);overflow-y:auto;background-color:rgb(var(--bg-color));border-radius:var(--brdr)}main.svelte-ycieuk .c-image.svelte-ycieuk.svelte-ycieuk{width:100%;height:200px;background-color:rgb(var(--bg-color-secondary));display:flex;align-items:center;justify-content:center;border-radius:var(--brdr);overflow:hidden}main.svelte-ycieuk .c-controls.svelte-ycieuk.svelte-ycieuk{width:100%;height:max-content;display:flex;align-items:center;justify-content:space-evenly}main.svelte-ycieuk .c-controls button.svelte-ycieuk.svelte-ycieuk{width:3.5em;height:3em;display:flex;align-items:center;justify-content:center;border:none;outline:none;background-color:transparent;border-radius:var(--brdr);cursor:pointer}main.svelte-ycieuk .c-controls div.svelte-ycieuk.svelte-ycieuk{display:flex;align-items:center;justify-content:center;position:relative}main.svelte-ycieuk .c-controls div .volume-control.svelte-ycieuk.svelte-ycieuk{background-color:rgb(var(--bg-color-secondary));position:absolute;top:105%;left:0;height:250px;width:3em;border-radius:var(--brdr);display:flex;align-items:center;justify-content:center;flex-direction:column;overflow:hidden;gap:var(--pd)}main.svelte-ycieuk .c-controls div .volume-control button.svelte-ycieuk.svelte-ycieuk{width:100%;height:4em;display:flex;align-items:center;justify-content:center;background-color:transparent;border:none;border-radius:0}main.svelte-ycieuk .c-controls div .volume-control input[type='range'].svelte-ycieuk.svelte-ycieuk{width:1.1em;height:calc(100% - 6em);appearance:none;-webkit-appearance:slider-vertical;background-color:rgba(0, 0, 0, 0.2);outline:none;border:none;overflow:hidden;border-radius:var(--brdr);cursor:pointer}main.svelte-ycieuk .c-controls div .volume-control input[type='range'].svelte-ycieuk.svelte-ycieuk::-webkit-slider-thumb{width:1.1em;height:1.1em;background-color:rgb(var(--orange));border:none;border-radius:50%;outline:none;box-shadow:0px 410px 0px 400px rgb(var(--orange));cursor:pointer}main.svelte-ycieuk .c-controls div .volume-control input[type='range'].svelte-ycieuk.svelte-ycieuk::-moz-range-thumb{width:1.1em;height:1.1em;background-color:rgb(var(--orange));border:none;outline:none;border-radius:50%;box-shadow:0px 410px 0px 400px rgb(var(--orange));cursor:pointer}main.svelte-ycieuk ul.details.svelte-ycieuk.svelte-ycieuk{width:100%;background-color:rgb(var(--bg-color-secondary));display:flex;align-items:center;justify-content:center;flex-direction:column;border-radius:var(--brdr);overflow:hidden}main.svelte-ycieuk ul.details li.svelte-ycieuk.svelte-ycieuk{width:100%;height:4em;list-style:none;display:flex;align-items:center;justify-content:space-between;padding:var(--pd);overflow-x:auto;text-align:right;gap:var(--pd)}main.svelte-ycieuk ul.details li.svelte-ycieuk+li.svelte-ycieuk{border-top:0.1em solid rgb(var(--bg-color))}main.svelte-ycieuk ul.details li p.svelte-ycieuk.svelte-ycieuk:last-child{text-transform:capitalize;width:max-content;white-space:nowrap;overflow:hidden;text-overflow:ellipsis}main.svelte-ycieuk .c-controls button.svelte-ycieuk.svelte-ycieuk:nth-child(2){width:5em;background-color:rgb(var(--bg-color-secondary))}@media(max-width: 600px){main.svelte-ycieuk.svelte-ycieuk.svelte-ycieuk{padding:var(--pd);position:fixed;right:0;top:0;bottom:0;border-top-right-radius:0;border-bottom-right-radius:0}}",
  map: null
};
const Audio_control = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  new RadioBrowserApi("My Radio App");
  let Open_audio_control = true;
  let Current_radio = {
    country: "",
    url_resolved: "",
    state: "",
    tags: "",
    favicon: "",
    language: "",
    bitrate: 0,
    paused: true,
    name: "",
    loading: true,
    country_code: "",
    home_page: "",
    favorite: false,
    station_id: "",
    codec: "",
    votes: 0
  };
  let volume = { volume: 100, openSlider: false, time: 0 };
  const logoRadio = {
    img: ``,
    failed: false,
    loading: false,
    loaded: false
  };
  $$result.css.add(css$1);
  {
    if (volume) {
      audio.subscribe((value) => {
        value.volume = volume.volume / 100;
        volume.volume = value.volume * 100;
      });
    }
  }
  {
    current_radio.subscribe((value) => {
      if (Current_radio !== value) {
        Current_radio = value;
        const img = new Image();
        img.src = Current_radio.favicon;
        logoRadio.loading = true;
        img.onload = () => {
          logoRadio.loading = false;
          logoRadio.loaded = true;
          logoRadio.img = `<img width="40" height="40" src="${Current_radio.favicon}"/>`;
        };
        img.onerror = () => {
          logoRadio.loading = false;
          logoRadio.failed = true;
          logoRadio.img = `<i class="bi bi-music-note"></i>`;
        };
      }
    });
  }
  {
    open_audio_control.subscribe((value) => {
      if (Open_audio_control !== value) {
        Open_audio_control = value;
      }
    });
  }
  return `${Open_audio_control ? `<main class="svelte-ycieuk"><div class="c-image svelte-ycieuk"><!-- HTML_TAG_START -->${logoRadio.img}<!-- HTML_TAG_END --></div>
		<h3 style="text-align: center;">${escape(Current_radio.name)}</h3>
		<div class="c-controls svelte-ycieuk"><div class="svelte-ycieuk"><button id="volume" class="wk-rp svelte-ycieuk"><i class="bi bi-volume-up-fill"></i></button>
				${``}</div>
			<button id="stop" class="wk-rp svelte-ycieuk">${Current_radio.loading ? `${validate_component(Loader, "Loader").$$render($$result, { isLoading: true }, {}, {})}` : `${Current_radio.paused ? `<i class="bi bi-play-fill"></i>` : `<i class="bi bi-stop-fill"></i>`}`}</button>
			<button id="favorite" class="wk-rp svelte-ycieuk">${`${Current_radio.favorite ? `
					<i class="bi bi-star-fill"></i>` : `
					<i class="bi bi-star"></i>`}`}</button></div>
		<h4 style="text-indent: 0.5em;align-self: flex-start;">Informations</h4>
		<ul class="details svelte-ycieuk"><li class="svelte-ycieuk"><p class="svelte-ycieuk">Language</p>
				<p class="svelte-ycieuk">${escape(Current_radio.language)}</p></li>
			<li class="svelte-ycieuk"><p class="svelte-ycieuk">Tags</p>
				<p class="svelte-ycieuk">${escape(Current_radio.tags)}</p></li>
			<li class="svelte-ycieuk"><p class="svelte-ycieuk">Votes</p>
				<p class="svelte-ycieuk">${escape(Current_radio.votes)}</p></li></ul>
		<h4 style="text-indent: 0.5em;align-self: flex-start;">Local</h4>
		<ul class="details svelte-ycieuk"><li class="svelte-ycieuk"><p class="svelte-ycieuk">Country</p>
				<p class="svelte-ycieuk">${escape(Current_radio.country)}</p></li></ul>
		<h4 style="text-indent: 0.5em;align-self: flex-start;">Audio</h4>
		<ul class="details svelte-ycieuk"><li class="svelte-ycieuk"><p class="svelte-ycieuk">Bitrate</p>
				<p class="svelte-ycieuk">${escape(Current_radio.bitrate)}</p></li>
			<li class="svelte-ycieuk"><p class="svelte-ycieuk">Codec</p>
				<p class="svelte-ycieuk">${escape(Current_radio.codec)}</p></li>
			<li class="svelte-ycieuk"><p class="svelte-ycieuk">Reproduction</p>
				<p class="svelte-ycieuk"><a${add_attribute("href", Current_radio.url_resolved, 0)} target="_blank">${escape(Current_radio.url_resolved)}</a></p></li></ul></main>` : ``}`;
});
const Overlay_svelte_svelte_type_style_lang = "";
const css = {
  code: "main.svelte-143vyjh{width:100%;height:100%;position:fixed;top:0;left:0;background-color:rgba(0, 0, 0, 0.2);display:none;z-index:1}@media(max-width: 600px){main.svelte-143vyjh{display:flex}}",
  map: null
};
const Overlay = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  let Open_audio_control = false;
  $$result.css.add(css);
  {
    open_audio_control.subscribe((value) => {
      if (Open_audio_control !== value) {
        Open_audio_control = value;
      }
    });
  }
  return `${Open_audio_control ? `<main class="svelte-143vyjh"></main>` : ``}`;
});
export {
  Audio_control as A,
  Mini_audio_control as M,
  Overlay as O
};
