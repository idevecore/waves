import { w as writable } from "./index.js";
import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { RadioBrowserApi } from "radio-browser-api";
const firebase_config = {
  apiKey: "AIzaSyAeXw4hEeKG1KFSf9BKRQ6oW0gCRVZ3uac",
  authDomain: "radio-fab7e.firebaseapp.com",
  projectId: "radio-fab7e",
  storageBucket: "radio-fab7e.appspot.com",
  messagingSenderId: "555159897716",
  appId: "1:555159897716:web:336b0de36b790764b6f321",
  measurementId: "G-LT1GH39PRK"
};
initializeApp(firebase_config);
getAuth();
const user_data = writable({
  uid: "",
  favorites: []
});
const radios = writable({
  top_votes: [],
  top_clicks: [],
  recent_clicks: [],
  searchs: []
});
const open_audio_control = writable(false);
const open_mini_audio_control = writable(false);
const open_search_component = writable(false);
const current_radio = writable({
  bitrate: 0,
  codec: "",
  country: "",
  country_code: "",
  favicon: "",
  home_page: "",
  language: "",
  name: "",
  state: "",
  station_id: "",
  tags: "",
  url_resolved: "",
  loading: false,
  paused: true,
  favorite: false,
  votes: 0
});
const audio = writable(null);
new RadioBrowserApi("My Radio App");
export {
  open_mini_audio_control as a,
  open_search_component as b,
  current_radio as c,
  audio as d,
  open_audio_control as o,
  radios as r,
  user_data as u
};
