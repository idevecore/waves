import { c as create_ssr_component, v as validate_component } from "../../chunks/index2.js";
import "firebase/auth";
import "../../chunks/stores.js";
import { L as Loader } from "../../chunks/Loader.js";
const styles = "";
const Loader_page_svelte_svelte_type_style_lang = "";
const css = {
  code: "section.svelte-imrnr7{display:flex;align-items:center;justify-content:center;position:relative;flex-direction:column;gap:var(--pd);background-color:rgb(var(--bg-color))}",
  map: null
};
let isLoading = true;
const Loader_page = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  $$result.css.add(css);
  return `<section class="svelte-imrnr7">${validate_component(Loader, "Loading").$$render($$result, { isLoading }, {}, {})}
	<h3>Loading...</h3>
</section>`;
});
const Layout = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  return `${`${validate_component(Loader_page, "LoaderPage").$$render($$result, {}, {}, {})}`}
<audio></audio>`;
});
export {
  Layout as default
};
