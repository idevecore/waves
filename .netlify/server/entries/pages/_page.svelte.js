import { c as create_ssr_component, v as validate_component, b as each, d as add_attribute, e as escape } from "../../chunks/index2.js";
import { u as user_data, o as open_audio_control, a as open_mini_audio_control } from "../../chunks/stores.js";
import { M as Mini_audio_control, A as Audio_control, O as Overlay } from "../../chunks/Overlay.js";
import { L as Loader } from "../../chunks/Loader.js";
const _page_svelte_svelte_type_style_lang = "";
const css = {
  code: "section.svelte-1jkjk1x.svelte-1jkjk1x.svelte-1jkjk1x{position:fixed;top:0;left:0;width:100%;display:flex;align-items:flex-start;justify-content:flex-start;height:100%;padding:var(--pd);background-color:rgb(var(--bg-color))}section.svelte-1jkjk1x .content.svelte-1jkjk1x.svelte-1jkjk1x{width:100%;height:100%;display:flex;align-items:center;justify-content:flex-start;flex-direction:column;border-radius:var(--brdr);padding-bottom:2.5em;overflow-y:auto}section.svelte-1jkjk1x .content header.svelte-1jkjk1x.svelte-1jkjk1x{width:calc(100%);height:4em;position:sticky;top:0;background-color:rgb(var(--bg-color-secondary));border-radius:var(--brdr);display:flex;align-items:center;justify-content:flex-start;padding:var(--pd);z-index:1;box-shadow:var(--box-shadow);transition:box-shadow ease-in-out 200ms}.shadow-on-scrool.svelte-1jkjk1x.svelte-1jkjk1x.svelte-1jkjk1x{box-shadow:var(--box-shadow)}section.svelte-1jkjk1x .content header button.svelte-1jkjk1x.svelte-1jkjk1x{width:4em;height:3.5em;display:flex;align-items:center;justify-content:center;border-radius:var(--brdr);background-color:transparent;border:none;cursor:pointer}section.svelte-1jkjk1x .content header h3.svelte-1jkjk1x.svelte-1jkjk1x{position:absolute;top:50%;left:50%;transform:translate(-50%, -50%)}.open_audio_control.svelte-1jkjk1x.svelte-1jkjk1x.svelte-1jkjk1x{width:calc(100% - 300px) !important}.open_mini_audio_control.svelte-1jkjk1x.svelte-1jkjk1x.svelte-1jkjk1x{height:calc(100% - 3em) !important}section.svelte-1jkjk1x .content .c-radio-list.svelte-1jkjk1x.svelte-1jkjk1x{width:100%;height:calc(100% - 150px);display:flex;align-items:flex-start;justify-content:flex-start;flex-direction:column;padding:var(--pd);border-radius:var(--brdr)}section.svelte-1jkjk1x .content .c-radio-list ul.svelte-1jkjk1x.svelte-1jkjk1x{width:100%;height:max-content;display:grid;grid-template-columns:31% 31% 31%;align-items:flex-start;justify-content:space-between;gap:var(--pd);padding-bottom:var(--pd)}section.svelte-1jkjk1x .content .c-radio-list ul button.discover.svelte-1jkjk1x.svelte-1jkjk1x{width:50%;height:4em;background-color:rgb(var(--orange));color:rgb(255, 255, 255);border:none;border-radius:var(--brdr);outline:none;cursor:pointer;display:flex;align-items:center;justify-content:center;position:absolute;top:50%;left:50%;transform:translate(-50%, -50%)}section.svelte-1jkjk1x .content .c-radio-list ul button.discover.svelte-1jkjk1x>.svelte-1jkjk1x{pointer-events:none}section.svelte-1jkjk1x .content .c-radio-list ul li.svelte-1jkjk1x.svelte-1jkjk1x{width:100%;height:4em;list-style:none;display:grid;grid-template-columns:70px calc(100% - 140px) 70px;align-items:center;justify-content:center;background-color:rgb(var(--bg-color-secondary));border-radius:var(--brdr);overflow:hidden;gap:var(--pd)}section.svelte-1jkjk1x .content .c-radio-list ul li .c-image.svelte-1jkjk1x.svelte-1jkjk1x{width:100%;height:100%;display:flex;align-items:center;justify-content:center}section.svelte-1jkjk1x .content .c-radio-list ul li .c-image img.svelte-1jkjk1x.svelte-1jkjk1x{width:100%;height:100%;display:block;margin-left:auto;margin-right:auto}section.svelte-1jkjk1x .content .c-radio-list ul li .c-image img.svelte-1jkjk1x.svelte-1jkjk1x::before{display:flex;align-items:center;justify-content:center}section.svelte-1jkjk1x .content .c-radio-list ul li button.svelte-1jkjk1x.svelte-1jkjk1x{width:3.5em;height:3em;border-radius:var(--brdr);border:none;display:flex;align-items:center;justify-content:center;background-color:transparent;cursor:pointer}@media(max-width: 1000px){section.svelte-1jkjk1x .content .c-radio-list ul.svelte-1jkjk1x.svelte-1jkjk1x{grid-template-columns:49% 49%}}@media(max-width: 600px){.open_audio_control.svelte-1jkjk1x.svelte-1jkjk1x.svelte-1jkjk1x{width:100% !important}section.svelte-1jkjk1x .content .c-radio-list ul.svelte-1jkjk1x.svelte-1jkjk1x{grid-template-columns:100%}}",
  map: null
};
const Page = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  let favorite_radios = [];
  let Open_audio_control = false;
  let Open_mini_audio_control = false;
  let loading = true;
  $$result.css.add(css);
  {
    user_data.subscribe((value) => {
      loading = true;
      if (favorite_radios !== value.favorites) {
        favorite_radios = value.favorites;
      }
      loading = false;
    });
  }
  {
    open_audio_control.subscribe((value) => {
      if (Open_audio_control !== value) {
        Open_audio_control = value;
      }
    });
  }
  {
    open_mini_audio_control.subscribe((value) => {
      if (Open_mini_audio_control !== value) {
        Open_mini_audio_control = value;
      }
    });
  }
  return `<section class="svelte-1jkjk1x"><div class="${[
    "content svelte-1jkjk1x",
    (Open_audio_control ? "open_audio_control" : "") + " " + (Open_mini_audio_control ? "open_mini_audio_control" : "")
  ].join(" ").trim()}"><header class="shadow-on-scrool svelte-1jkjk1x"><button class="wk-rp svelte-1jkjk1x"><i class="bi bi-plus-lg"></i></button>
			<h3 class="svelte-1jkjk1x">Favorites</h3></header>
		<div class="c-radio-list svelte-1jkjk1x">${loading ? `<div style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%); display: flex; align-items: center; justify-content: center; flex-direction: column; gap: var(--pd)">${validate_component(Loader, "Loader").$$render($$result, { isLoading: true }, {}, {})}
					<h3 class="svelte-1jkjk1x">Loading...</h3></div>` : `<ul class="svelte-1jkjk1x">${favorite_radios.length > 0 ? `${each(favorite_radios, (radioStation) => {
    return `<li class="c-radio-list-li svelte-1jkjk1x"><div class="c-image svelte-1jkjk1x"><img alt="image"${add_attribute("src", radioStation.favicon, 0)} class="svelte-1jkjk1x"></div>
								<p>${escape(radioStation.name)}</p>
								<button class="wk-rp svelte-1jkjk1x"><i class="bi bi-play-fill"></i></button>
							</li>`;
  })}` : `<button class="discover wk-rp light svelte-1jkjk1x"><p class="p svelte-1jkjk1x">Discover new stations</p></button>`}</ul>`}</div></div>
	${validate_component(Mini_audio_control, "MiniAudioControl").$$render($$result, {}, {}, {})}
	${validate_component(Audio_control, "AudioControl").$$render($$result, {}, {}, {})}
	${validate_component(Overlay, "Overlay").$$render($$result, {}, {}, {})}
</section>`;
});
export {
  Page as default
};
