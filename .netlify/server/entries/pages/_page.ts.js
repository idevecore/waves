import { u as user_data } from "../../chunks/stores.js";
function load() {
  let data;
  user_data.subscribe((value) => {
    data = value;
  });
  return data;
}
export {
  load
};
