import { c as create_ssr_component, f as compute_rest_props, h as createEventDispatcher, i as spread, j as escape_attribute_value, k as escape_object, d as add_attribute, v as validate_component, b as each, e as escape } from "../../../chunks/index2.js";
import { o as open_audio_control, a as open_mini_audio_control, r as radios, b as open_search_component } from "../../../chunks/stores.js";
import { A as Audio_control, M as Mini_audio_control, O as Overlay } from "../../../chunks/Overlay.js";
const splide_min = "";
const splideSkyblue_min = "";
const splideSeaGreen_min = "";
const splideCore_min = "";
function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}
function forOwn(object, iteratee) {
  if (object) {
    const keys = Object.keys(object);
    for (let i = 0; i < keys.length; i++) {
      const key = keys[i];
      if (key !== "__proto__") {
        if (iteratee(object[key], key) === false) {
          break;
        }
      }
    }
  }
  return object;
}
function isObject(subject) {
  return subject !== null && typeof subject === "object";
}
function isEqualDeep(subject1, subject2) {
  if (Array.isArray(subject1) && Array.isArray(subject2)) {
    return subject1.length === subject2.length && !subject1.some((elm, index) => !isEqualDeep(elm, subject2[index]));
  }
  if (isObject(subject1) && isObject(subject2)) {
    const keys1 = Object.keys(subject1);
    const keys2 = Object.keys(subject2);
    return keys1.length === keys2.length && !keys1.some((key) => {
      return !Object.prototype.hasOwnProperty.call(subject2, key) || !isEqualDeep(subject1[key], subject2[key]);
    });
  }
  return subject1 === subject2;
}
function merge(object, source) {
  const merged = object;
  forOwn(source, (value, key) => {
    if (Array.isArray(value)) {
      merged[key] = value.slice();
    } else if (isObject(value)) {
      merged[key] = merge(isObject(merged[key]) ? merged[key] : {}, value);
    } else {
      merged[key] = value;
    }
  });
  return merged;
}
function slice(arrayLike, start, end) {
  return Array.prototype.slice.call(arrayLike, start, end);
}
function apply(func) {
  return func.bind.apply(func, [null].concat(slice(arguments, 1)));
}
function typeOf(type, subject) {
  return typeof subject === type;
}
apply(typeOf, "function");
apply(typeOf, "string");
apply(typeOf, "undefined");
const Splide_1 = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  let $$restProps = compute_rest_props($$props, ["class", "options", "splide", "extensions", "transition", "hasTrack", "go", "sync"]);
  let { class: className = void 0 } = $$props;
  let { options = {} } = $$props;
  let { splide = void 0 } = $$props;
  let { extensions = void 0 } = $$props;
  let { transition = void 0 } = $$props;
  let { hasTrack = true } = $$props;
  createEventDispatcher();
  let root;
  let prevOptions = merge({}, options);
  function go(control) {
    splide?.go(control);
  }
  function sync(target) {
    splide?.sync(target);
  }
  if ($$props.class === void 0 && $$bindings.class && className !== void 0)
    $$bindings.class(className);
  if ($$props.options === void 0 && $$bindings.options && options !== void 0)
    $$bindings.options(options);
  if ($$props.splide === void 0 && $$bindings.splide && splide !== void 0)
    $$bindings.splide(splide);
  if ($$props.extensions === void 0 && $$bindings.extensions && extensions !== void 0)
    $$bindings.extensions(extensions);
  if ($$props.transition === void 0 && $$bindings.transition && transition !== void 0)
    $$bindings.transition(transition);
  if ($$props.hasTrack === void 0 && $$bindings.hasTrack && hasTrack !== void 0)
    $$bindings.hasTrack(hasTrack);
  if ($$props.go === void 0 && $$bindings.go && go !== void 0)
    $$bindings.go(go);
  if ($$props.sync === void 0 && $$bindings.sync && sync !== void 0)
    $$bindings.sync(sync);
  {
    if (splide && !isEqualDeep(prevOptions, options)) {
      splide.options = options;
      prevOptions = merge({}, prevOptions);
    }
  }
  return `

<div${spread(
    [
      {
        class: escape_attribute_value(classNames("splide", className))
      },
      escape_object($$restProps)
    ],
    {}
  )}${add_attribute("this", root, 0)}>${hasTrack ? `${validate_component(SplideTrack, "SplideTrack").$$render($$result, {}, {}, {
    default: () => {
      return `${slots.default ? slots.default({}) : ``}`;
    }
  })}` : `${slots.default ? slots.default({}) : ``}`}</div>`;
});
const SplideTrack = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  let $$restProps = compute_rest_props($$props, ["class"]);
  let { class: className = void 0 } = $$props;
  if ($$props.class === void 0 && $$bindings.class && className !== void 0)
    $$bindings.class(className);
  return `<div${spread(
    [
      {
        class: escape_attribute_value(classNames("splide__track", className))
      },
      escape_object($$restProps)
    ],
    {}
  )}><ul class="splide__list">${slots.default ? slots.default({}) : ``}</ul></div>`;
});
const SplideSlide = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  let $$restProps = compute_rest_props($$props, ["class"]);
  let { class: className = void 0 } = $$props;
  if ($$props.class === void 0 && $$bindings.class && className !== void 0)
    $$bindings.class(className);
  return `<li${spread(
    [
      {
        class: escape_attribute_value(classNames("splide__slide", className))
      },
      escape_object($$restProps)
    ],
    {}
  )}>${slots.default ? slots.default({}) : ``}</li>`;
});
const _page_svelte_svelte_type_style_lang = "";
const css = {
  code: "section.svelte-1hofk8x.svelte-1hofk8x{position:fixed;top:0;left:0;width:100%;display:flex;align-items:flex-start;justify-content:flex-start;height:100%;padding:var(--pd);background-color:rgb(var(--bg-color))}section.svelte-1hofk8x .content.svelte-1hofk8x{width:100%;height:100%;display:flex;align-items:center;justify-content:flex-start;flex-direction:column;border-radius:var(--brdr);overflow-y:auto}.open_audio_control.svelte-1hofk8x.svelte-1hofk8x{width:calc(100% - 300px) !important}.open_mini_audio_control.svelte-1hofk8x.svelte-1hofk8x{height:calc(100% - 3em) !important}.c-slider.svelte-1hofk8x.svelte-1hofk8x{width:100%;height:150px;background-color:rgb(var(--blue));border-radius:var(--brdr);padding:var(--pd);position:relative}.c-slider.svelte-1hofk8x button.search.svelte-1hofk8x{position:absolute;top:var(--pd);right:var(--pd);width:3em;height:3em;display:flex;align-items:center;justify-content:center;border:none;border-radius:50%;cursor:pointer;background-color:transparent;color:rgb(255, 255, 255);z-index:1}.slider__like-button.svelte-1hofk8x.svelte-1hofk8x{background-color:rgba(0, 0, 0, 0.3);width:200px;height:2.5em;border-radius:var(--brdr);display:flex;align-items:center;justify-content:center;border:none;outline:none;cursor:pointer}section.svelte-1hofk8x .content .c-radio-list.svelte-1hofk8x{width:100%;height:calc(100% - 150px);display:flex;align-items:flex-start;justify-content:flex-start;flex-direction:column;padding:var(--pd);gap:var(--pd)}section.svelte-1hofk8x .content .c-radio-list ul.svelte-1hofk8x{width:100%;height:max-content;display:grid;grid-template-columns:31% 31% 31%;align-items:flex-start;justify-content:space-between;gap:var(--pd)}section.svelte-1hofk8x .content .c-radio-list ul li.svelte-1hofk8x{width:100%;height:4em;list-style:none;display:grid;grid-template-columns:70px calc(100% - 140px) 70px;align-items:center;justify-content:center;background-color:rgb(var(--bg-color-secondary));border-radius:var(--brdr);overflow:hidden;gap:var(--pd)}section.svelte-1hofk8x .content .c-radio-list ul li .c-image.svelte-1hofk8x{width:100%;height:100%;display:flex;align-items:center;justify-content:center}section.svelte-1hofk8x .content .c-radio-list ul li .c-image img.svelte-1hofk8x{width:100%;height:100%;display:block;margin-left:auto;margin-right:auto}section.svelte-1hofk8x .content .c-radio-list ul li .c-image img.svelte-1hofk8x::before{display:flex;align-items:center;justify-content:center}section.svelte-1hofk8x .content .c-radio-list ul li button.svelte-1hofk8x{width:3.5em;height:3em;border-radius:var(--brdr);border:none;display:flex;align-items:center;justify-content:center;background-color:transparent;cursor:pointer}@media(max-width: 1000px){section.svelte-1hofk8x .content .c-radio-list ul.svelte-1hofk8x{grid-template-columns:49% 49%}}@media(max-width: 600px){.open_audio_control.svelte-1hofk8x.svelte-1hofk8x{width:100% !important}section.svelte-1hofk8x .content .c-radio-list ul.svelte-1hofk8x{grid-template-columns:100%}}",
  map: null
};
const Page = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  let Open_audio_control = false;
  let Open_mini_audio_control = false;
  let Radios = {
    top_clicks: [],
    recent_clicks: [],
    top_votes: [],
    searchs: []
  };
  $$result.css.add(css);
  {
    open_audio_control.subscribe((value) => {
      if (Open_audio_control !== value) {
        Open_audio_control = value;
      }
    });
  }
  {
    open_mini_audio_control.subscribe((value) => {
      if (Open_mini_audio_control !== value) {
        Open_mini_audio_control = value;
      }
    });
  }
  {
    radios.subscribe((value) => {
      Radios = { ...value };
    });
  }
  {
    open_search_component.subscribe((value) => {
    });
  }
  return `<section class="svelte-1hofk8x"><div class="${[
    "content svelte-1hofk8x",
    (Open_audio_control ? "open_audio_control" : "") + " " + (Open_mini_audio_control ? "open_mini_audio_control" : "")
  ].join(" ").trim()}"><div class="c-slider svelte-1hofk8x"><button class="wk-rp light search svelte-1hofk8x"><i class="bi bi-search light"></i></button>
			${validate_component(Splide_1, "Splide").$$render(
    $$result,
    {
      hasTrack: false,
      options: {
        autoplay: true,
        type: "loop",
        classes: {
          page: "splide__pagination__page pagination__page-progress"
        }
      },
      style: "padding: 2em;"
    },
    {},
    {
      default: () => {
        return `${validate_component(SplideTrack, "SplideTrack").$$render($$result, {}, {}, {
          default: () => {
            return `${validate_component(SplideSlide, "SplideSlide").$$render(
              $$result,
              {
                style: "display: flex; align-items: center; justify-content: center;"
              },
              {},
              {
                default: () => {
                  return `<h4 class="light-color">Browse over 30,000 radio stations</h4>`;
                }
              }
            )}
					${validate_component(SplideSlide, "SplideSlide").$$render(
              $$result,
              {
                style: "text-align: center; display: flex; align-items: center; justify-content: center; flex-direction: column; padding: 0;"
              },
              {},
              {
                default: () => {
                  return `<h4 class="light-color">Provided by radio-browser.info</h4>
						<a style="text-decoration: none;" href="https://www.radio-browser.info/" target="_blank" class="slider__like-button light-color svelte-1hofk8x">Open site</a>`;
                }
              }
            )}
					${validate_component(SplideSlide, "SplideSlide").$$render(
              $$result,
              {
                style: "text-align: center; display: flex; align-items: center; justify-content: center; flex-direction: column; padding: 0;"
              },
              {},
              {
                default: () => {
                  return `<h4 class="light-color">See your favorite stations at:</h4>
						<button class="slider__like-button light-color svelte-1hofk8x">Favorites</button>`;
                }
              }
            )}`;
          }
        })}
				<div class="splide__arrows"><button class="splide__arrow splide__arrow--prev svelte-1hofk8x"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><g data-name="Layer 2"><g data-name="chevron-right"><rect width="24" height="24" transform="rotate(-90 12 12)" opacity="0"></rect><path d="M10.5 17a1 1 0 0 1-.71-.29 1 1 0 0 1 0-1.42L13.1 12 9.92 8.69a1 1 0 0 1 0-1.41 1 1 0 0 1 1.42 0l3.86 4a1 1 0 0 1 0 1.4l-4 4a1 1 0 0 1-.7.32z" fill="rgb(var(--orange))"></path></g></g></svg></button>
					<button class="splide__arrow splide__arrow--next svelte-1hofk8x"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><g data-name="Layer 2"><g data-name="chevron-right"><rect width="24" height="24" transform="rotate(-90 12 12)" opacity="0"></rect><path d="M10.5 17a1 1 0 0 1-.71-.29 1 1 0 0 1 0-1.42L13.1 12 9.92 8.69a1 1 0 0 1 0-1.41 1 1 0 0 1 1.42 0l3.86 4a1 1 0 0 1 0 1.4l-4 4a1 1 0 0 1-.7.32z" fill="rgb(var(--orange))"></path></g></g></svg></button></div>`;
      }
    }
  )}</div>
		<div class="c-radio-list svelte-1hofk8x"><h3>Most voted</h3>
			<ul class="svelte-1hofk8x">${each(Radios.top_votes, (radioStation) => {
    return `<li class="svelte-1hofk8x"><div class="c-image svelte-1hofk8x"><img alt="image"${add_attribute("src", radioStation.favicon, 0)} class="svelte-1hofk8x"></div>
						<p>${escape(radioStation.name)}</p>
						<button class="wk-rp svelte-1hofk8x"><i class="bi bi-play-fill"></i></button>
					</li>`;
  })}</ul>
			<h3>In High</h3>
			<ul class="svelte-1hofk8x">${each(Radios.top_clicks, (radioStation) => {
    return `<li class="svelte-1hofk8x"><div class="c-image svelte-1hofk8x"><img alt="image"${add_attribute("src", radioStation.favicon, 0)} class="svelte-1hofk8x"></div>
						<p>${escape(radioStation.name)}</p>
						<button class="wk-rp svelte-1hofk8x"><i class="bi bi-play-fill"></i></button>
					</li>`;
  })}</ul>
			<h3>Recent Stations</h3>
			<ul class="svelte-1hofk8x">${each(Radios.recent_clicks, (radioStation) => {
    return `<li class="svelte-1hofk8x"><div class="c-image svelte-1hofk8x"><img alt="image"${add_attribute("src", radioStation.favicon, 0)} class="svelte-1hofk8x"></div>
						<p>${escape(radioStation.name)}</p>
						<button class="wk-rp svelte-1hofk8x"><i class="bi bi-play-fill"></i></button>
					</li>`;
  })}</ul></div></div>
	${validate_component(Audio_control, "AudioControl").$$render($$result, {}, {}, {})}
	${validate_component(Mini_audio_control, "MiniAudioControl").$$render($$result, {}, {}, {})}
	${validate_component(Overlay, "Overlay").$$render($$result, {}, {}, {})}
</section>`;
});
export {
  Page as default
};
