

export const index = 5;
export const component = async () => (await import('../entries/pages/search/_page.svelte.js')).default;
export const file = '_app/immutable/entry/search-page.svelte.2baefe08.js';
export const imports = ["_app/immutable/entry/search-page.svelte.2baefe08.js","_app/immutable/chunks/index.fac930cf.js","_app/immutable/chunks/Overlay.3d2e9c13.js","_app/immutable/chunks/ripples.ca471da7.js","_app/immutable/chunks/stores.4adf857c.js","_app/immutable/chunks/index.0799413d.js","_app/immutable/chunks/Loader.c1b062ab.js","_app/immutable/chunks/singletons.32b2e86d.js"];
export const stylesheets = ["_app/immutable/assets/_page.856bbf37.css","_app/immutable/assets/Overlay.932db50b.css","_app/immutable/assets/Loader.5e00817d.css"];
export const fonts = [];
