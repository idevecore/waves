import {
  current_radio,
  open_mini_audio_control,
  open_audio_control,
  audio,
} from './stores'
import type { Current_station } from './Types'

export default class Audio {
  current_radio: Current_station
  constructor() {
    this.current_radio = {
      codec: '',
      bitrate: 0,
      station_id: '',
      language: '',
      favicon: '',
      tags: '',
      state: '',
      home_page: '',
      country_code: '',
      country: '',
      loading: false,
      name: '',
      paused: true,
      url_resolved: '',
      votes: 0,
    }
  }
  public init_radio({ radio_station }: any) {
    this.current_radio = {
      codec: radio_station.codec,
      bitrate: radio_station.bitrate,
      station_id: radio_station.id,
      language: radio_station.language,
      favicon: radio_station.favicon,
      tags: radio_station.tags,
      state: radio_station.state,
      home_page: radio_station.homepage,
      country_code: radio_station.countryCode,
      country: radio_station.country,
      loading: true,
      name: radio_station.name,
      paused: false,
      votes: radio_station.votes,
      url_resolved: radio_station.urlResolved
    }

    open_audio_control.update(() => true)
    current_radio.update(() => this.current_radio)
    open_mini_audio_control.update(() => false)
    audio.subscribe((value: HTMLAudioElement) => {
      value.src = this.current_radio.url_resolved
      value.play()
      value.addEventListener('canplaythrough', () => {
        current_radio.update((current_radio) => ({ ...current_radio, loading: false }))
      })
    })
  }
  public play_pause() {
    audio.subscribe((value: HTMLAudioElement) => {
      value.paused ? value.play() : value.pause()
      current_radio.update((current_radio) => ({ ...current_radio, paused: value.paused }))
    })
  }
}
