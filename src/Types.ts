export type Current_station = {
  bitrate: number
  codec: string
  country: string
  country_code: string
  favicon: string
  home_page: string
  language: string
  name: string
  state: string
  station_id: string
  tags: string
  url_resolved: string
  loading: boolean
  paused: boolean
  votes: number
}
