import { writable } from 'svelte/store'
import { RadioBrowserApi } from 'radio-browser-api';

export const radios = writable({
  top_votes: [],
  top_clicks: [],
  recent_clicks: [],
  searchs: []
})

export const open_audio_control = writable(false)
export const open_mini_audio_control = writable(false)
export const open_search_component = writable(false)
export const current_radio = writable({
  bitrate: 0,
  codec: '',
  country: '',
  country_code: '',
  favicon: '',
  home_page: '',
  language: '',
  name: '',
  state: '',
  station_id: '',
  tags: '',
  url_resolved: '',
  loading: false,
  paused: true,
  votes: 0,
})
export const audio: any = writable(null)
export const api = new RadioBrowserApi('My Radio App');